<?php

class Category {

    public $name;
    public $products;

    public function __construct($name) {
        $this->name = $name;
    }

    public function addProduct($product) {
        $this->products[] = $product;
    }

    public function getProducts() {
        return $this->products;
    }

}

