<?php

require_once __DIR__ . '/../vendor/common.php';

class Ex4Tests extends UnitTestCaseExtended {

    function categoryObjectCountIsCorrect() {
        $categoryList = [];

        require 'ex4.php';

        $this->assertEqual(sizeof($categoryList), 3);
    }

    function eachCategoryContainsCorrectProducts() {

        $categoryList = [];

        require 'ex4.php';

        $cat1Products = $this->getProductNumbersByCategoryName($categoryList, 'cat1');
        $cat3Products = $this->getProductNumbersByCategoryName($categoryList, 'cat3');
        $cat5Products = $this->getProductNumbersByCategoryName($categoryList, 'cat5');

        $this->assertEqual($cat1Products, ['p2', 'p4', 'p5']);
        $this->assertEqual($cat3Products, ['p6', 'p8']);
        $this->assertEqual($cat5Products, ['p1', 'p3', 'p7']);
    }

    private function getProductNumbersByCategoryName($categoryList, $categoryName) : array {
        foreach ($categoryList as $category) {
            if ($category->name !== $categoryName) {
                continue;
            }

            $productNumbers = array_map(function ($each) {
                return $each->number;
            }, $category->getProducts());

            sort($productNumbers);

            return $productNumbers;
        }

        throw new RuntimeException('did not find category: ' . $categoryName);
    }


}

(new Ex4Tests())->run(new PointsReporter(
    [1 => 1,
     2 => 10]));

