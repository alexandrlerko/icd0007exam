<?php

require_once '../vendor/common.php';

const BASE_URL = 'http://localhost:8080';

class Ex1Tests extends WebTestCaseExtended {

    function indexToC() {
        $this->fetch('/');

        $this->ensureRelativeLink("c.html");
        $this->clickLink("c.html");
        $this->assertCurrentUrlEndsWith("c.html");
    }

    function cToA() {
        $this->fetch('/a/b/c/c.html');

        $this->ensureRelativeLink("a.html");
        $this->clickLink("a.html");
        $this->assertCurrentUrlEndsWith("a.html");
    }

    function aToE() {
        $this->fetch('/a/a.html');

        $this->ensureRelativeLink("e.html");
        $this->clickLink("e.html");
        $this->assertCurrentUrlEndsWith("e.html");
    }

    function eToF() {
        $this->fetch('/a/b/c/d/e/e.html');

        $this->ensureRelativeLink("f.html");
        $this->clickLink("f.html");
        $this->assertCurrentUrlEndsWith("f.html");
    }

    function fToD() {
        $this->fetch('/a/b/c/d/e/f/f.html');

        $this->ensureRelativeLink("d.html");
        $this->clickLink("d.html");
        $this->assertCurrentUrlEndsWith("d.html");
    }

    private function fetch($url) {
        $this->get(BASE_URL . $url);
    }
}

(new Ex1Tests())->run(new PointsReporter(
    [1 => 1,
     2 => 2,
     3 => 3,
     4 => 4,
     5 => 6]));
