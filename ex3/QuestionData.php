<?php

class QuestionData {

    private $questions = [];
    private $answers = [];

    public function addQuestion($question) {
        return $this->questions[] = $question;
    }

    public function getQuestionByNumber($number) {
        return $this->questions[intval($number) - 1];
    }

    public function setAnswerByNumber($number, $answer) {
        return $this->answers[intval($number) - 1] = $answer;
    }

    public function isLastQuestion($number) {
        return intval($number) >= count($this->questions);
    }

    public function __toString() {
        $result = '';

        foreach ($this->questions as $index => $question) {
            $answer = $this->answers[$index] ?? '';
            $result .= sprintf("%s %s\n", $question, $answer);
        }

        return $result;
    }

}

