<?php

require_once '../vendor/common.php';

const BASE_URL = 'http://localhost:8080';

class Ex3Tests extends WebTestCaseExtended {

    function defaultPageIsFrom() {
        $this->get(BASE_URL);

        $this->assertField('answer');
    }

    function showsQuestionsOneAtTheTime() {
        $this->get(BASE_URL);

        $this->assertText('Kui palju on 2+2?');
        $this->assertNoText('Kui palju on 6+8?');

        $this->clickSubmitByName('nextButton');

        $this->assertText('Kui palju on 6+8?');
        $this->assertNoText('Kui palju on 2+2?');

        $this->clickSubmitByName('nextButton');

        $this->assertText('Kui palju on 7+2?');
        $this->assertNoText('Kui palju on 6+8?');
    }

    function gathersAnswersOneAtTheTime() {
        $this->get(BASE_URL);

        $answer1 = $this->getRandomValue();
        $this->setFieldByName('answer', $answer1);
        $this->clickSubmitByName('nextButton');

        $answer2 = $this->getRandomValue();
        $this->setFieldByName('answer', $answer2);
        $this->clickSubmitByName('nextButton');

        $answer3 = $this->getRandomValue();
        $this->setFieldByName('answer', $answer3);
        $this->clickSubmitByName('nextButton');

        $this->clickSubmitByName('finishButton');

        $this->assertText($answer1);
        $this->assertText($answer2);
        $this->assertText($answer3);
    }

    private function getRandomValue() {
        return substr(md5(mt_rand()), 0, 9);
    }

}


(new Ex3Tests())->run(new PointsReporter(
    [1 => 0,
     2 => 4,
     3 => 10]));


