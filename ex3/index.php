<?php

require_once '../vendor/tpl.php';
require_once 'QuestionData.php';

if (isset($_POST['finishButton'])) {

    $questionData = new QuestionData(); // lihtsalt näidis väärtus

    // kood tuleb siia

    print renderTemplate('main.html', [
        'fileName' => 'result.html',
        'result' => $questionData->__toString() ]);

} else if (isset($_POST['nextButton'])) {

    // kood tuleb siia

    $templateData = [];
    $templateData['fileName'] = 'question-form.html';
    $templateData['questionData'] = new QuestionData(); // lihtsalt näidis väärtus

    // kood tuleb siia

    print renderTemplate('main.html', $templateData);

} else if (empty($_POST)) {

    $questionData = new QuestionData();
    $questionData->addQuestion('Kui palju on 2+2?');
    $questionData->addQuestion('Kui palju on 6+8?');
    $questionData->addQuestion('Kui palju on 7+2?');

    $templateData = [];
    $templateData['fileName'] = 'question-form.html';
    $templateData['questionData'] = $questionData;
    $templateData['serializedData'] = urlencode(serialize($questionData));
    $templateData['questionNumber'] = 1;

    print renderTemplate('main.html', $templateData);
} else {
    throw new RuntimeException('bad command');
}
